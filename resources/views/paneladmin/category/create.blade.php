@extends('paneladmin.template')

@section('content')
<section class="">
    <div class="head4">
        <div class="row">
            <div class="col s12 m12 l12">
			 <div class="container text-center">
                <div class="page-header">
                    <h1>
                    CATEGORÍAS <small>[Agregar categoría]</small>
                    </h1>
                </div>
             </div>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s6 offset-m3">
                <div class="page">
                    
                    @if (count($errors) > 0)
                        @include('panel.partials.errors')
                    @endif
                    
                    {!! Form::open(['route'=>'category.store']) !!}
                    
                        <div>
                          <label for="name">Nombre:</label>
                                
                                {!! 
                                    Form::text(
                                        'name', 
                                        null, 
                                        array(
                                            'class'=>'form-control',
                                            'placeholder' => 'Ingresa la categoria...',
                                            'autofocus' => 'autofocus'
                                        )
                                    ) 
                                !!}  
                        </div>

                        <div >
                            <label for="description">Descripción:</label>
                            
                            {!! 
                                Form::textarea(
                                    'description', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => '...',

                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                            {!! Form::submit('Guardar', array('class'=>'btn btn-primary')) !!}
                            <a href="{{ route('category.index') }}" class="btn btn-warning">Cancelar</a>
                        </div>
                    
                    {!! Form::close() !!}
                    </div>
                    

                </div>
            </div>
        </div>   
        
    </div>
</section>
	

		
        

@stop


        