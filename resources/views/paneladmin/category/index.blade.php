@extends('paneladmin.template')
  
@section('content')
<section class="">
  <div class="head4">
    <div class="row">
        <div class="col s12 m12 l12">
			<div class="container text-center">
			<div class="page-header">
				<h1>
					CATEGORÍAS <a href="{{ route('category.create') }}" class="waves-effect waves-light btn-small"><i class="material-icons left">add_circle_outline</i>Categorias</a>
				</h1>
			</div>
			<div class="page">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Editar</th>
								<th>Eliminar</th>
								<th>Nombre</th>
								<th>Descripción</th>
							</tr>
						</thead>
						<tbody>
							@foreach($categories as $category)
								<tr>
									<td>
									<a href="{{ route('category.edit', $category) }}" class="btn-floating btn-large waves-effect waves-light blue">
									<i class="material-icons left">brush</i></a>
									</td>
									
									<td>
									{!! Form::open(['route' => ['category.destroy', $category]]) !!}
        								<input type="hidden" name="_method" value="DELETE">
        								<button  class="btn-floating btn-large waves-effect waves-light red">
										    <i class="material-icons left">delete_forever</i>
        								</button>
        							{!! Form::close() !!}
								</td>
									<td>{{ $category->name }}</td>
									<td>{{ $category->description }}</td>
									
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>
        </div>
    </div>
  </div>
</section>

	
	
@stop