@extends('paneladmin.template')
  
@section('content')
<section class="">
  <div class="head4">
    <div class="row">
        <div class="col s12 m12 l12">
			<div class="container text-center">
				<div class="page-header">
					<h1>
						USUARIOS <a href="{{ route('user.create') }}" class="waves-effect waves-light btn-small"><i class="material-icons left">add_circle_outline</i>Usuarios</a>
					</h1>
				</div>
				<div class="page">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Editar</th>
									<th>Eliminar</th>
									<th>Nombre</th>
									<th>Apellidos</th>
									<th>Usuario</th>
									<th>Correo</th>
									<th>Tipo</th>
									<th>Activo</th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $user)
									<tr>
										<td>
										<a href="{{ route('user.edit', $user) }}" class="btn-floating btn-large waves-effect waves-light blue">
										<i class="material-icons left">brush</i></a>
										</td>
										
										<td>
										{!! Form::open(['route' => ['user.destroy', $user]]) !!}
											<input type="hidden" name="_method" value="DELETE">
											<button  class="btn-floating btn-large waves-effect waves-light red">
												<i class="material-icons left">delete_forever</i>
											</button>
										{!! Form::close() !!}
										</td>
										<td>{{ $user->name }}</td>
										<td>{{ $user->last_name }}</td>
										<td>{{ $user->user }}</td>
										<td>{{ $user->email }}</td>
										<td>{{ $user->type }}</td>
										<td>{{ $user->active == 1 ? "Si" : "No" }}</td>
										
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>

		    </div>
        </div>
    </div>
  </div>
</section>
@stop