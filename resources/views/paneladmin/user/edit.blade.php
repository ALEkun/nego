@extends('paneladmin.template')

@section('content')
<section class="">
    <div class="head4">
        <div class="row">
            <div class="col s12 m12 l12">
			 <div class="container text-center">
                <div class="page-header">
                    <h1>
                    USUARIOS <small>[ Editar usuario ]</small>
                    </h1>
                </div>
             </div>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s6 offset-m3">
                <div class="page">
                    
                    @if (count($errors) > 0)
                        @include('panel.partials.errors')
                    @endif
                    {!! Form::model($user, array('route' => array('user.update', $user))) !!}
                    <input type="hidden" name="_method" value="PUT">
                        <div>
                            <label for="name">Nombre:</label>
                            
                            {!! 
                                Form::text(
                                    'name', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa el nombre...',
                                        'autofocus' => 'autofocus',
                                        //'required' => 'required'
                                    )
                                ) 
                            !!}  
                        </div>

                        <div class="form-group">
                            <label for="last_name">Apellidos:</label>
                            
                            {!! 
                                Form::text(
                                    'last_name', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa los apellidos...',
                                        //'required' => 'required'
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                            <label for="email">Correo:</label>
                            
                            {!! 
                                Form::text(
                                    'email', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa el correo...',
                                        //'required' => 'required'
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                            <label for="user">Usuario:</label>
                            
                            {!! 
                                Form::text(
                                    'user', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa el nombre de usuario...',
                                        //'required' => 'required'
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                            <label for="password">Password:</label>
                            
                            {!! 
                                Form::password(
                                    'password', 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresauna contraseña...',
                                        //'required' => 'required'
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                            <label for="confirm_password">Confirmar Password:</label>
                            
                            {!! 
                                Form::password(
                                    'password_confirmation',
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'confirma la contraseña...',
                                        //'required' => 'required'
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                        <label for="type">Tipo:</label>
                        
                          <p>
                            <label>
                                <input class="with-gap" name="type" type="radio" value="user" checked />
                                <span>Usuario</span>
                            </label>
                          </p>
                          <p>
                            <label>
                                <input class="with-gap" name="type" type="radio" value="admin" />
                                <span>Admin</span>
                            </label>
                           </p>
                        </div>
                        
                        <div class="form-group">
                            <label for="address">Dirección:</label>
                            
                            {!! 
                                Form::textarea(
                                    'address', 
                                    null, 
                                    array(
                                        'class'=>'form-control'
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                        <label for="type">Active:  </label>
                        <label>
                            <input type="checkbox" name="active" value="active" checked="checked"/>
                             <span></span>
                        </label>   
                        
                        
                        <div class="form-group">
                            {!! Form::submit('Actualizar', array('class'=>'btn btn-primary')) !!}
                            <a href="{{ route('user.index') }}" class="btn btn-warning">Cancelar</a>
                        </div>
                    
                    {!! Form::close() !!}
                    </div>
                    

                </div>
            </div>
        </div>   
        
    </div>
</section>
	

		
        

@stop