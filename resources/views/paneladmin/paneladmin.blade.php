@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">PANEL DE ADMINISTRACION</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="col m6">
                        <div class="panel">
                            <i class="fa fa-list-alt icon-home"></i>
                            <a href="{{ route('category.index') }}" class="btn btn-warning btn-block btn-home-admin">CATEGORÍAS</a>
                        </div>
                    </div>

                    <div class="col m6">
                        <div class="panel">
                            <i class="fa fa-shopping-cart  icon-home"></i>
                            <a href="{{ route('user.index') }}" class="btn btn-warning btn-block btn-home-admin">USUARIOS</a>
                        </div>
                    </div>

                    <div class="col m6">
                        <div class="panel">
                            <i class="fa fa-shopping-cart  icon-home"></i>
                            <a href="{{ route('business.index') }}" class="btn btn-warning btn-block btn-home-admin">negocios</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     
</div>
@endsection