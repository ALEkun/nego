<ul id="dropdown2" class="dropdown-content">
  
  <li><a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesion') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
</ul>
<li>   @if (Route::has('login'))
                <div class="top-right links">
                
                    @auth
                        @if (auth()->user()->type == 'admin') <li>
            	
                            <a href="{{ route('paneladmin') }}"
                            onclick="submit();">
                            panel administacion
                            </a>

                            @else 
                            
                            <a href="{{ route('paneluser') }}">+Sube tu negocio</a>
                            </li>
                        @endif 

                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown2">{{ Auth::user()->user }}<i class="material-icons right">arrow_drop_down</i></a></li>
                    @else
                       <li> <a href="{{ route('login') }}">Login</a></li>
                       <li> <a href="{{ route('register') }}">Registrarse</a></li>
                    @endauth
                </div>
            @endif
            </li>

           