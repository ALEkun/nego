@extends('home.template')

@section('title','Inicio | NEGOMEX')

@section('content')


<section class="sec1">
  <div class="head">
    <div class="row">
          <div class="col s12 m12 l12">
            <div class="center prom2"> 
              <p class="txt">Resultados de busqueda</p>
            </div>
            </div>
          </div>
      </div>
  </div>
</section>

<section class="sec2">
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="center prom2"> 
          
        </div>
      </div>
    </div>
    <div class="contain text-center">
      <div class="row">
             


          <div id="bizs" class="col s10 m10 l10">
                @foreach($search as $business)
                    <div class="product white-panel">
                        <p class="detalle4">{{ $business->name }}</p><hr>
                        <img src="{{ $business->image }}" width="200">
                        <div class="product-info panel">
                            <p class="detalle3">{{ $business->extract }}</p>
                            <p>
                                <a href="{{ route('biz-detail', $business->slug) }}" class="waves-effect waves-light btn-small"><i class="material-icons left">add_circle_outline</i>Leer mas</a>
                            </p>
                        </div>
                    </div>
                @endforeach
          </div>

      </div>
    </div>
</section>
@endsection