@extends('home.template')

@section('content')

<section class="sec1">
  <div class="head">
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="center prom2"> 
              <p class="txt">Detalle de {{ $biz->name }}</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="sec2">
    <div class="row">
      <div class="col s4 m4 l4">
        <div class="center prom2"> 
              <p><img src="{{ $biz->image }}" width="250"></p>
        </div>
      </div>
      <div class="col s8 m8 l8">
        <div class=" prom2"> 
              <p class="detalle1">{{ $biz->name }}</p>
              <p class="detalle2">{{ $biz->description }}</p>
              <p class="detalle1">CONTACTANOS:</p>
              <p class="detalle2">Telefono:</p>
              <p class="detalle3">{{ $biz->phone }}</p>
              <p class="detalle2">Direccion:</p>
              <p class="detalle3">{{ $biz->address }}</p>
              <p class="detalle2">Correo Electronico:</p>
              <p class="detalle3">{{ $biz->email }}</p>
              
              
        </div>
        <div class=" prom2"> 
              <p class="detalle2">Categoria:</p>
              <p class="detalle3">{{ $biz->category->name }}</p>
        </div>
      </div>
    </div>
    <p class="center prom2">
           <a href="{{ route('catalogo') }}" class="waves-effect waves-light btn-small"><i class="material-icons left">cached</i>Regresar</a>
        </p>
</section>
							
    

@stop