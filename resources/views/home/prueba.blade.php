@extends('home.template')

@section('content')

<section class="sec1">
  <div class="head">
    <div class="row">
          <div class="col s12 m12 l12">
            <div class="center prom2"> 
              <p class="txt">Detalle del producto</p>
            </div>
            </div>
          </div>
      </div>
  </div>
</section>
       @foreach($bizs as $business)
								<tr>
									
                                    <td><img src="{{ $business->image }}" width="40"></td>
                                    <td>{{ $business->name }}</td>
                                    <td>{{ $business->category->name }}</td>
                                    <td>{{ $business->extract }}</td>
                                    <td>{{ $business->user_id }}: {{ $business->user->name }}</td>
                                    <td>{{ $business->visible == 1 ? "Si" : "No" }}</td>
									
								</tr>
							@endforeach
    <p>
        <a href="{{ route('catalogo') }}" class="waves-effect waves-light btn-small"><i class="material-icons left">return</i>Regresar</a>
    </p>
</div>
@stop