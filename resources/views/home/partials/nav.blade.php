
<ul id="dropdown1" class="dropdown-content">
  <li><a href="#!">one</a></li>
  <li><a href="#!">two</a></li>
  <li class="divider"></li>
  <li><a href="#!">three</a></li>
</ul>
<nav>

<div class="nav-wrapper">
  <a href="/" class="brand-logo">NegoMEX</a>
  <ul id="nav-mobile" class="right hide-on-med-and-down">
    <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
    <li><a href="{{ url('') }}">Blog</a></li>
    <li><a href="{{ route('catalogo') }}">Negocios</a></li>
    @include('home.partials.menu-user')

  </ul>
</div>
</nav>

   