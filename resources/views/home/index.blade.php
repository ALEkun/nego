@extends('home.template')

@section('title','Inicio | NEGOMEX')

@section('content')


<section class="sec1">
  <div class="head">
    <div class="row">
          <div class="col s12 m12 l12">
            <div class="center prom2"> 
              <p class="txt">EXPLORA TU CIUDAD</p>
              <p class="txt1">Negomex ayuda a encontrar negocios locales en tu ciudad, </p>
              <p class="txt1">Vamos Explora Ahora. </p>
            </div>

           
              <div class="row">
                <div class="col s8 offset-s2">
                {{  Form::open(['url'=>'search', 'method' => 'GET']) }}
                  <div class="row">
                    <div class="center prom2">
                      <div class="input-field col s4">
                          <label for="name">:</label>
                                
                                {{ 
                                    Form::text(
                                        'name', 
                                        null, 
                                        array(
                                            'class'=>'form-control',
                                            'placeholder' => 'que estas buscando',
                                            'autofocus' => 'autofocus'
                                        )
                                    ) 
                                }}  
                      </div>
                      <div class="input-field col s4">
                          <label for="address">:</label>
                                
                                {{ 
                                    Form::text(
                                        'address', 
                                        null, 
                                        array(
                                            'class'=>'form-control',
                                            'placeholder' => 'que estas buscando',
                                            'autofocus' => 'autofocus'
                                        )
                                    ) 
                                }}  
                      </div>
                      <div class="input-field col s4">
                        <select>
                          <option value="" disabled selected><p class="txt1">Todas las categorías</p></option>
                          <option value="1">Option 1</option>
                          <option value="2">Option 2</option>
                          <option value="3">Option 3</option>
                        </select>
                      </div>
                      <div class="center"> <button class="btn waves-effect waves-light indigo" type="submit" name="action">Buscar
                        <i class="material-icons right">send</i></div>
                      </div>
                      
                  </div> 
                  {{ Form::close() }} 
                 </div>
                <!--<div class="row">
            <div class="input-field col s6 offset-m3">
                <div class="page">
                    
                    {{  Form::open(['url'=>'search', 'method' => 'GET']) }}
                        
                        
                        
                        
                        <div>
                            <label for="name">:</label>
                                
                                {{ 
                                    Form::text(
                                        'name', 
                                        null, 
                                        array(
                                            'class'=>'form-control',
                                            'placeholder' => 'que estas buscando',
                                            'autofocus' => 'autofocus'
                                        )
                                    ) 
                                }}  
                        </div>

                        
                        
                        <div class="form-group">
                            {{ Form::submit('Buscar', array('class'=>'btn waves-effect waves-light indigo')) }}
                          </div>
                    
                    {{ Form::close() }}
                    </div>
                    

                </div>
            </div>
        </div> -->
              </div>
            </div>
          </div>
      </div>
  </div>
</section>

<section class="sec2">
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="center prom2"> 
          <p class="txt2">Qué quieres hacer esta noche</p>
          <p class="txt3">Descubre y conéctate con los negocios de tu ciudad o estado</p>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
      <li><a href="{{ route('catalogo') }}">Negocios</a></li>
        <div class="col s12 m3"><img width='200px' height='200px' src="img/guadalajara.jpg"></div>
        <div class="col s12 m3 "><img width='200px' height='200px' src="img/morelia.jpg"></div>
        <div class="col s12 m3 "><img width='200px' height='200px' src="img/monteerey.jpg"></div>
        <div class="col s12 m3 "><img width='200px' height='200px' src="img/paracho.jpg"></div>
      </div>
    </div>
</section>

<section class="sec3">
  <div class="head1">
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="center prom2"> 
          <p class="txt4">Las cosas más populares que hacer en la ciudad</p>
          <p class="txt5">Descubre algunos de los más populares,</p>
          <p class="txt5">basados ​​en las reseñas de usuarios y calificaciones.</p>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col s12 m4"><img width='300px' height='300px' src="img/restaurante.jpg"></div>
        <div class="col s12 m4"><img width='300px' height='300px' src="img/hotel.jpg"></div>
        <div class="col s12 m4"><img width='300px' height='300px' src="img/bares.jpg"></div>
      </div>
    </div>
  </div>
</section>

<section class="sec4">
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="center prom2"> 
          <p class="txt2">Como lo hacemos</p>
          <p class="txt3">Conectando negocios con miembros</p>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="center"> 
          <div class="container-encontrar">
            <div class="col s4 m4 "> <img  src="img/buscarini.jpg">
              <p class="txt6">ENCONTRAR NEGOCIOS</p>
              <p class="txt7">Descubrir y conectar con negocios locales en su estado, como
                dentistas, peluqueros, restaurantes y muchos más</p>
            </div>
          </div>

          <div class="container-optener">
            <div class="col s4 m4 "><img  src="img/optener.jpg">
              <p class="txt6">OBTENER EXPERIENCIA</p>
              <p class="txt7">Obtener información valiosa sobre los anuncios y decirles a otros lectores sobre sus experiencias al dejar comentarios para los negocios.</p>
            </div>
          </div>

          <div class="container-crear">
            <div class="col s4 m4"><img src="img/crear.jpg">
              <p class="txt6">CREAR RECERVACIONES</p>
              <p class="txt7">Configurar fácilmente una cita directamente de la página del listado de negocios usando nuestras opciones de reserva integrados.</p>
            </div>
          </div>
        </div>  
      </div>
    </div>
</section>

<section class="sec1">
  <div class="head2">
    <div class="row">
          <div class="col s12 m12 l12">
            <div class="center"> 
              <p class="txt">OPTENGA EXPOSICIÓN COMERCIAL</p>
              <p class="txt1">NegoMEX te da la mejor forma de encontrar negocios en tu ciudad. </p>
                <button class="btn waves-effect waves-light indigo" type="submit" name="action">¡Crea tu cuenta ahora!
                  </button>
              <p class="txt1">Y podrás tenerla en minutos </p>
            </div>

            </div>
          </div>
      </div>
  </div>
</section>
<section>

@endsection