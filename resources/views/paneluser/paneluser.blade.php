@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
            </div>
            
            <div class="table-responsive">
				<h3>Datos del usuario</h3>
				<table class="table table-striped table-hover table-bordered">
					<tr><td>Nombre:</td><td>{{ Auth::user()->name . " " . Auth::user()->last_name }}</td></tr>
					<tr><td>Usuario:</td><td>{{ Auth::user()->user }}</td></tr>
					<tr><td>Correo:</td><td>{{ Auth::user()->email }}</td></tr>
					<tr><td>Dirección:</td><td>{{ Auth::user()->address }}</td></tr>
				</table>
            </div>
        </div>

        <div class="col s12 m12 l12">
			<div class="container text-center">
			<div class="page-header">
				<h1>
					NEGOCIOS <a href="{{ route('paneluser.create') }}" class="waves-effect waves-light btn-small"><i class="material-icons left">add_circle_outline</i>Negocios</a>
				</h1>
			</div>
			<div class="page">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<thead>
                            <tr>
                                <th>Editar</th>
                                <th>Eliminar</th>
                                <th>Imagen</th>
                                <th>Nombre</th>
                                <th>Categoría</th>
                                <th>Extracto</th>
                                <th>usuario</th>
                                <th>Visible</th>
                            </tr>
						</thead>
						<tbody>
							@foreach($bizs as $business)
								<tr>
									<td>
									<a href="{{ route('paneluser.edit', $business) }}" class="btn-floating btn-large waves-effect waves-light blue">
									<i class="material-icons left">brush</i></a>
									</td>
									
									<td>
										{!! Form::open(['route' => ['paneluser.destroy', $business]]) !!}
											<input type="hidden" name="_method" value="DELETE">
											<button  class="btn-floating btn-large waves-effect waves-light red">
												<i class="material-icons left">delete_forever</i>
											</button>
										{!! Form::close() !!}
									</td>
                                    <td><img src="{{ $business->image }}" width="40"></td>
                                    <td>{{ $business->name }}</td>
                                    <td>{{ $business->category->name }}</td>
                                    <td>{{ $business->extract }}</td>
									<td>{{ $business->user->name }}</td>
                                    <td>{{ $business->visible == 1 ? "Si" : "No" }}</td>
									
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
                <hr>
            
            <?php echo $bizs->render(); ?>
			</div>

		</div>
        </div>
    </div>
</div>
@endsection