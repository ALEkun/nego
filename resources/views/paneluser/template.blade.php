

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>@yield('title','negomex')</title>

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="{{ asset('css/styles.css') }}"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>
<body>


    @include('partials.nav')

    @yield('content')

    @include('partials.footer')
    
  
  <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
  <script src="{{ asset('js/materialize.min.js') }}"></script>
  <script type="text/javascript" charset="utf-8">
      $(document).ready(function(){
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
      });

       $(document).ready(function(){
          $('select').formSelect();
        });

  </script>
</body>
</html>
