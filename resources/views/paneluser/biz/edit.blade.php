@extends('paneluser.template')

@section('content')

<section class="">
    <div class="head4">
        <div class="row">
            <div class="col s12 m12 l12">
			 <div class="container text-center">
                <div class="page-header">
                    <h1>
                    NEGOCIOS <small>[editar negocio]</small>
                    </h1>
                </div>
             </div>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s6 offset-m3">
                <div class="page">
                    
                    @if (count($errors) > 0)
                        @include('partials.errors')
                    @endif
                    
                    {!! Form::model($business, array('route' => array('paneluser.update', $business))) !!}
                    <input type="hidden" name="_method" value="PUT">
                        <div>
                            <label class="" for="category_id">Categoría</label>
                            {!! Form::select('category_id', 
                                              $categories, 
                                              555, 
                                              ['class' => 'form-control cate',
                                              'placeholder' => 'Selecciona una de las categorias',]) !!}
                        </div>
                        
                        
                        
                        <div>
                            <label for="name">Nombre:</label>
                                
                                {!! 
                                    Form::text(
                                        'name', 
                                        null, 
                                        array(
                                            'class'=>'form-control',
                                            'placeholder' => 'Ingresa el nombre...',
                                            'autofocus' => 'autofocus'
                                        )
                                    ) 
                                !!}  
                        </div>

                        <div >
                            <label for="extract">Extracto:</label>
                                
                                {!! 
                                    Form::text(
                                        'extract', 
                                        null, 
                                        array(
                                            'class'=>'form-control',
                                            'placeholder' => 'Ingresa un pequeño extracto de la descripcion...',
                                        )
                                    ) 
                                !!}
                        </div>

                        <div class="form-group">
                            <label for="description">Descripción:</label>
                            
                            {!! 
                                Form::textarea(
                                    'description', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa la descripcion completa...',
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                            <label for="address">Direccion:</label>
                            
                            {!! 
                                Form::text(
                                    'address', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa la direccion del negocio...',
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                            <label for="email">Correo Electronico:</label>
                            
                            {!! 
                                Form::text(
                                    'email', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa el correo electronico de tu negocio...',
                                    )
                                ) 
                            !!}
                        </div>

                        <div class="form-group">
                            <label for="phone">Telefono:</label>
                            
                            {!! 
                                Form::text(
                                    'phone', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa el telefono...',
                                    )
                                ) 
                            !!}
                        </div>

                        <div class="form-group">
                            <label for="image">Imagen:</label>
                            
                            {!! 
                                Form::text(
                                    'image', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa la url de la imagen...',
                                    )
                                ) 
                            !!}
                        </div>
                        
                       

                        <div class="form-group">
                        <label for="visible">Visible:  </label>
                        
                        <label>
                            <input type="checkbox" name="visible" value="visible" checked="checked"/>
                             <span></span>
                        </label>
                           
                        </div>
                        
                        <div class="form-group">
                            {!! Form::submit('Actualizar', array('class'=>'btn btn-primary')) !!}
                            <a href="{{ route('paneluser.index') }}" class="btn btn-warning">Cancelar</a>
                        </div>
                    
                    {!! Form::close() !!}
                    </div>
                    

                </div>
            </div>
        </div>   
        
    </div>
</section>


		
        

@stop


        