<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Requests\SaveBizRequest;
use App\Http\Controllers\Controller;
use App\Biz;
use App\Category;
use Illuminate\support\Facades\storage;
use App\User;
use Illuminate\Support\Facades\File;
class BusinessController extends Controller
{
    public function index()
    {
        $bizs = Biz::orderBy('id', 'desc')->paginate();
        //$bizs = Biz::where('user_id', '=', 1)->get();
        $categories = Category::orderBy('id', 'desc')->pluck('name', 'id');
        $users = User::orderBy('id', 'desc')->pluck('name', 'id');
        //dd($bizs);
        return view('paneladmin.biz.index', compact('bizs','users','categories'));
    }

    public function create()
    {
        $categories = Category::orderBy('id', 'desc')->pluck('name', 'id');
        $users = User::orderBy('id', 'desc')->pluck('name', 'id');
        //dd($categories);

        return view('paneladmin.biz.create', compact('categories','users'));
    }

    public function store(SaveBizRequest $request)
    {
        $data = [
            'name'          => $request->get('name'),
            'slug'          => str_slug($request->get('name')),
            'description'   => $request->get('description'),
            'extract'       => $request->get('extract'),
            'address'       => $request->get('address'),
            'email'         => $request->get('email'),
            'phone'         => $request->get('phone'),
            
            'visible'       => $request->has('visible') ? 1 : 0,
            'category_id'   => $request->get('category_id'),
            'user_id'       => $request->get('user_id')
        ];

        $business = Biz::create($data);
        /*if($request->hasFile('image')){
            $filenamewithextension =$request->('image')->getClientOriginalName();
            $filename =pathinfo(filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filenametostore = $filename.'.'.$extension;
            $ruta = storage::disk('public')->putFileAs('bussines',$request->file(image),$ $filenametostore);
            $bizs->fill([image => asset(ruta)])->save();
        }

        if($request->hasFile('image')){
            $filenamewithextension = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $ruta = storage::disk('public')->putFileAs('bussines',$request->file('image'),$filenametostore);
            $bizs->fill(['image' => asset($ruta)])->save();

        }*/

        if($request->hasFile('image')) {
            //get filename with extension
            $filenamewithextension = $request->file('image')->getClientOriginalName();
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
            //Upload 
            $ruta = Storage::disk('public')->putFileAs('bussines',$request->file('image'),$filenametostore); 
            $business->fill(['image' => asset($ruta)])->save();
           
        }

        $message = $business ? 'Negocio agregado correctamente!' : 'El negocio NO pudo agregarse!';
        
        return redirect()->route('business.index')->with('message', $message);
    }

    public function edit(Biz $business)
    {
        $categories = Category::orderBy('id', 'desc')->pluck('name', 'id');

        return view('paneladmin.biz.edit', compact('categories', 'business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(SaveBizRequest $request, $id)
    {
        $business= Biz::find($id);
       
       /* if(!empty($business->image) ){
               // $path =$business->image;
           // if (! Storage::exists($business->image)) {   
               // $dir_upload = public_path('bussines/');
               //Storage::delete($business->image);
               $path =Storage::disk('public')->delete($business->image);
               //\File::delete($filename);
               //unlink($path);
              // $images=Storage::delete($path);
               dd($path);
                //return response()->json($images);
            }*/
           

        //dd($business->image);    
       // $business->update($request->all());

        $business->fill($request->all());
        $business->slug = str_slug($request->get('name'));
        $business->visible = $request->has('visible') ? 1 : 0;
          
            

        if($request->hasFile('image')) {
            //$images = Biz::find($id);
           //$dir_upload = public_path('bussines/');
            
            //get filename with extension
            $filenamewithextension = $request->file('image')->getClientOriginalName();
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
            //Upload 
            $ruta = Storage::disk('public')->putFileAs('bussines',$request->file('image'),$filenametostore); 
            $business->fill(['image' => asset($ruta)]);
            $business->save();
            //$business->image=asset($ruta)->save();
        }
        

        /*if($request->hasFile('image')) {
            //get filename with extension
            $filenamewithextension = $request->file('image')->getClientOriginalName();
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
            //Upload 
            $ruta = Storage::disk('public')->putFileAs('bussines',$request->file('image'),$filenametostore); 
            $business->fill(['image' => asset($ruta)]);
            $business->save();
            //$business->image=asset($ruta)->save();
        }*/
        //dd($business);

       /*if ($request->hasFile('image')) {
            $original_filename = $request->file('image')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $filename = pathinfo($original_filename, PATHINFO_FILENAME);
            $file_ext = end($original_filename_arr);
            $destination_path = './bussines';
            $image = $filename.'_'.time().'.'.$file_ext;
            if ($request->file('image')->move($destination_path, $image)) {
                //$category->logo = '/logo' . $image;
                $business->image = url('/bussines/'.$image);
                $business->save();
                //$category->fill(['logo' => asset($image)])->save();
            } else {
                return response()->json(['message' => 'Cannot upload file'], 400);
            }
        } else {
            return response()->json(['message' => 'File not found'], 400);
        }*/
        
        $updated = $business->save();
        
        $message = $updated ? 'Negocio actualizado correctamente!' : 'El negocio NO pudo actualizarse!';
        
        return redirect()->route('business.index')->with('message', $message);
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Biz  $business)
    {
        $deleted = $business->delete();
        
        $message = $deleted ? 'Negocio eliminado correctamente!' : 'El negocio NO pudo eliminarse!';
        
        return redirect()->route('business.index')->with('message', $message);
    }
}
