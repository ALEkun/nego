<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biz;
use App\Category;
use App\User;

class PanelUserController extends Controller
{
    public function index()
    {
        //$bizs = Biz::find(1);
        //$bizs = Biz::where('user_id', '=', 1)->get();
        $bizs = Biz::orderBy('id', 'desc')->paginate();
        $bizs -> each(function ($bizs){
            $bizs -> category;
            $bizs -> user;
        });
        //dd($bizs);
        return view('paneluser.paneluser')
                  -> with('bizs', $bizs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::orderBy('id', 'desc')->pluck('name', 'id');
        $users = User::orderBy('id', 'desc')->pluck('name', 'id');
        //dd($categories);
      


        return view('paneluser.biz.create', compact('categories','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */

    public function store(SaveBizRequest $request)
    {
        $data = [
            'name'          => $request->get('name'),
            'slug'          => str_slug($request->get('name')),
            'description'   => $request->get('description'),
            'extract'       => $request->get('extract'),
            'address'       => $request->get('address'),
            'email'         => $request->get('email'),
            'phone'         => $request->get('phone'),
            'image'         => $request->get('image'),
            'visible'       => $request->has('visible') ? 1 : 0,
            'category_id'   => $request->get('category_id'),
            'user_id'       => $request->get('user_id')
        ];

        $business = Biz::create($data);

        $message = $business ? 'Negocio agregado correctamente!' : 'El negocio NO pudo agregarse!';
        
        return redirect()->route('paneluser.index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    /**public function show(Biz $business)
    {
        return $business;
    }

    
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Biz $business)
    {
        $categories = Category::orderBy('id', 'desc')->pluck('name', 'id');

        return view('paneluser.paneluser', compact('categories', 'business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(SaveBizRequest $request, Biz $business)
    {
        

        $business->fill($request->all());
        $business->slug = str_slug($request->get('name'));
        $business->visible = $request->has('visible') ? 1 : 0;
        
        $updated = $business->save();
        
        $message = $updated ? 'Negocio actualizado correctamente!' : 'El negocio NO pudo actualizarse!';
        
        return redirect()->route('paneluser.index')->with('message', $message);
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */

    public function destroy(Biz  $business)
    {
        $deleted = $business->delete();
        
        $message = $deleted ? 'Negocio eliminado correctamente!' : 'El negocio NO pudo eliminarse!';
        
        return redirect()->route('paneluser.index')->with('message', $message);
    }
}
