<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biz;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**public function __construct()
    {
        $this->middleware('auth');
    }

    
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home/index');
    }

    public function Catalogo()
    {
       $bizs = Biz::all();
       $categories = Category::all();
       //dd($bizs,$categories);
       return view('home.catalogo', compact('bizs'), compact('categories'));
    }

    public function show($slug)
    {
    	$biz = Biz::where('slug', $slug)->first();
    	//dd($biz);
        return view('home.show', compact('biz'));
    }
}
