<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biz extends Model
{
    protected $table = 'bizs';

    protected $fillable = ['name', 'slug', 'description', 'extract', 'email', 'address', 'phone',  'image', 'visible', 
                           'category_id','user_id'];

    
    // Relation with Category
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    // Relation with USer
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeSearch($query, $name)
    {

            return $query->where('name', 'LIKE', "%$name%")
                         ->orWhere('description', 'LIKE', "%$name%" );
        
        
    }
}
