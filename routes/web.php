<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/',[ 
    'as' => 'home',
    'uses' => 'HomeController@index'
    ]);
Route::get('paneladmin', 'PanelAdminController@index')->name('paneladmin');
Route::resource('paneluser', 'PanelUserController');

Route::get('catalogo',[ 
    'as' => 'catalogo',
    'uses' => 'HomeController@catalogo'
    ]);

Route::get('biz/{slug}', [
    'as' => 'biz-detail',
    'uses' => 'HomeController@show'
    ]);     
   
Auth::routes();
Route::get('search/','SearchController@search');

// Category dependency injection
Route::bind('category', function($category){
    return App\Category::find($category);
});

// User dependency injection
Route::bind('user', function($user){
    return App\User::find($user);
});

// User dependency injection
Route::bind('biz', function($biz){
    return App\Biz::find($biz);
});


//ADMIN

Route::resource('category', 'Panel\CategoryController');
Route::resource('user', 'Panel\UserController');
Route::resource('business', 'Panel\BusinessController');
