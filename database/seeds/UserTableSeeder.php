<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
			[
				'name' 		=> 'geno', 
				'last_name' => 'hurtado', 
				'email' 	=> 'geno@hotmail.com', 
				'user' 		=> 'geno',
				'password' 	=> \Hash::make('123456'),
				'type' 		=> 'admin',
				'active' 	=> 1,
				'address' 	=> 'cheran',
				'created_at'=> new DateTime,
				'updated_at'=> new DateTime
			],
			[
				'name' 		=> 'alekun', 
				'last_name' => 'torres', 
				'email' 	=> 'alekun@hotmail.com', 
				'user' 		=> 'alekun',
				'password' 	=> \Hash::make('123456'),
				'type' 		=> 'user',
				'active' 	=> 1,
				'address' 	=> 'San Cosme 290, Cuauhtemoc, D.F.',
				'created_at'=> new DateTime,
				'updated_at'=> new DateTime
			],
			[
				'name' 		=> 'roberto', 
				'last_name' => 'rodriguez', 
				'email' 	=> 'roberto@hotmail.com', 
				'user' 		=> 'roberto',
				'password' 	=> \Hash::make('123456'),
				'type' 		=> 'user',
				'active' 	=> 1,
				'address' 	=> 'jalisco',
				'created_at'=> new DateTime,
				'updated_at'=> new DateTime
			],
			
		);
		User::insert($data);
    }
}
