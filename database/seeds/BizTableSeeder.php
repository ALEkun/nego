<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
 
use App\Biz;
 
class BizTableSeeder extends Seeder {
 
	/**
	 * Run the Bizs table seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$data = array(
			[
				'name' 			=> 'leopard',
				'slug' 			=> 'leopard',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'image' 		=> 'http://www.truffleshuffle.co.uk/store/images_high_res/Mens_Red_Batman_Graffiti_Logo_T_Shirt_hi_res.jpg',
				'visible' 		=> 1,
				'email' 	    => 'geno@hotmail.com',
				'address' 	    => 'cheran', 
				'phone' 	    => '5143535353534',
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'user_id' 	    => 2,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'restaurante',
				'slug' 			=> 'restaurante',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'image' 		=> 'http://www.truffleshuffle.co.uk/store/images_high_res/Mens_Red_Batman_Graffiti_Logo_T_Shirt_hi_res.jpg',
				'visible' 		=> 1,
				'email' 	    => 'geno@hotmail.com',
				'address' 	    => 'cheran', 
				'phone' 	    => '5143535353534',
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'user_id' 	    => 2,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'negocio1',
				'slug' 			=> 'negocio1',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'image' 		=> 'http://www.truffleshuffle.co.uk/store/images_high_res/Mens_Red_Batman_Graffiti_Logo_T_Shirt_hi_res.jpg',
				'visible' 		=> 1,
				'email' 	    => 'geno@hotmail.com',
				'address' 	    => 'cheran', 
				'phone' 	    => '5143535353534',
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'user_id' 	    => 3,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'negocio2',
				'slug' 			=> 'negocio2',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'image' 		=> 'http://www.truffleshuffle.co.uk/store/images_high_res/Mens_Red_Batman_Graffiti_Logo_T_Shirt_hi_res.jpg',
				'visible' 		=> 1,
				'email' 	    => 'geno@hotmail.com',
				'address' 	    => 'cheran', 
				'phone' 	    => '5143535353534',
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'user_id' 	    => 2,
				'category_id' 	=> 2
			],
			[
				'name' 			=> 'negocio3',
				'slug' 			=> 'negocio3',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'image' 		=> 'http://www.truffleshuffle.co.uk/store/images_high_res/Mens_Red_Batman_Graffiti_Logo_T_Shirt_hi_res.jpg',
				'visible' 		=> 1,
				'email' 	    => 'geno@hotmail.com',
				'address' 	    => 'cheran', 
				'phone' 	    => '5143535353534',
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'user_id' 	    => 3,
				'category_id' 	=> 2
			],
			[
				'name' 			=> 'negocio4',
				'slug' 			=> 'negocio4',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'image' 		=> 'http://www.truffleshuffle.co.uk/store/images_high_res/Mens_Red_Batman_Graffiti_Logo_T_Shirt_hi_res.jpg',
				'visible' 		=> 1,
				'email' 	    => 'geno@hotmail.com',
				'address' 	    => 'cheran', 
				'phone' 	    => '5143535353534',
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'user_id' 	    => 3,
				'category_id' 	=> 2
			],
			[
				'name' 			=> 'negocio5',
				'slug' 			=> 'negocio5',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'image' 		=> 'http://www.truffleshuffle.co.uk/store/images_high_res/Mens_Red_Batman_Graffiti_Logo_T_Shirt_hi_res.jpg',
				'visible' 		=> 1,
				'email' 	    => 'geno@hotmail.com',
				'address' 	    => 'cheran', 
				'phone' 	    => '5143535353534',
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'user_id' 	    => 2,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'negocio6',
				'slug' 			=> 'negocio6',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'image' 		=> 'http://www.truffleshuffle.co.uk/store/images_high_res/Mens_Red_Batman_Graffiti_Logo_T_Shirt_hi_res.jpg',
				'visible' 		=> 1,
				'email' 	    => 'geno@hotmail.com',
				'address' 	    => 'cheran', 
				'phone' 	    => '5143535353534',
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'user_id' 	    => 3,
				'category_id' 	=> 2
			],
			
		);
 
		Biz::insert($data);
 
	}
 
}